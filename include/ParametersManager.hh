#ifndef LAB2_PARAMETERS_MANAGER
#define LAB2_PARAMETERS_MANAGER

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Default parameters values: */
#define DET_TRANS_DEFAULT         -4
#define NAME_AIR_DEFAULT          "G4_AIR"
#define NAME_SA_MATERIAL_DEFAULT  "G4_WATER"
#define NAME_DET_MATERIAL_DEFAULT "G4_WATER"

class ParametersManager
{
private:
  double m_detTrans;
  char *m_nameAir;
  char *m_nameSaMaterial;
  char *m_nameDetMaterial;

public:
  ParametersManager();
  ~ParametersManager();

  void set_detTrans(double detTrans);
  double get_detTrans(void) const;

  void set_nameAir(const char *nameAir);
  char *get_nameAir(void) const;

  void set_nameSaMaterial(const char *nameSaMaterial);
  char *get_nameSaMaterial(void) const;

  void set_nameDetMaterial(const char *nameDetMaterial);
  char *get_nameDetMaterial(void) const;
};

extern ParametersManager *paramManager;

#endif /* LAB2_PARAMETERS_MANAGER */
