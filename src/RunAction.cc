#include "RunAction.hh"
#include "Hist1i.h"
#include "G4Run.hh"
#include "Randomize.hh"
#include "ParametersManager.hh"
#include <iosfwd>

RunAction::RunAction() {}

RunAction::~RunAction() {}

extern G4double totalEnergy;
extern double detTrans;

bool ifFileOpened = false;
std::ofstream File;
//std::ofstream File("total_energy.txt");

void RunAction::BeginOfRunAction(const G4Run*)
{
  // ������� �����������
  // �� 0 �� 2000, � 2000 �������
  //hist = new Hist1i(0, 2000, 2000);
}

void RunAction::FillHist(G4double energy)
{
  //if (energy > 0) {
    // ��������� ����������� ��������� ������� � ���
    //hist->fill(energy/keV);
    //hist->fill(energy/keV + G4RandGauss::shoot(0, 33));
  //}
}

void RunAction::EndOfRunAction(const G4Run* )
{
    // ��������� ����������� � ����
    // ������ �������� - ������ ������ �����
    //hist->save("spectrum.csv", "\"energy, keV\", N");

    if (!ifFileOpened)
    {
      char *name = (char*) alloca(1024 * sizeof(char));
      sprintf(name, "total_energy_%lf_%s_%s_%s.txt",
        paramManager->get_detTrans(),
        paramManager->get_nameAir(),
        paramManager->get_nameSaMaterial(),
        paramManager->get_nameDetMaterial());
      File.open(name, std::ios::app);
      ifFileOpened = true;
    }
    File << totalEnergy << G4endl;
}

