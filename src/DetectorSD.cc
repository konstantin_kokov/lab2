#include "DetectorSD.hh"
#include "RunAction.hh"

#include "G4RunManager.hh"
#include "G4Step.hh"

using namespace CLHEP;

    G4double totalEnergy = 0;

DetectorSD::DetectorSD(G4String name): G4VSensitiveDetector(name)
{
  // �������� ��������� �� ����� RunAction
  // �� ����� �������� ��� ����� RunAction::FillHist
  // ��� ���������� ����������� ������� ����������� �������
  runAction = (RunAction*) G4RunManager::GetRunManager()->GetUserRunAction();
}

DetectorSD::~DetectorSD() {}

void DetectorSD::Initialize(G4HCofThisEvent*)
{
  // � ������ ������� ���������� ������� ����������� ����������
  //detEnergy = 0;
}

G4bool DetectorSD::ProcessHits(G4Step* step, G4TouchableHistory*)
{
  // ����� �������� ����� �������
  // ��������� ������� ���������� ��������
  // � �������� ������� ���������
  G4double edep = step->GetTotalEnergyDeposit()/MeV;
  //detEnergy += edep;
  totalEnergy += edep;

//  G4Track* track = step->GetTrack();
//  G4String particleName = track->GetDefinition()->GetParticleName();
//  if (particleName == "gamma")
//    return false;

  return true;
}

void DetectorSD::EndOfEvent(G4HCofThisEvent*)
{
  // ��������� ������� ����������� �� ������� � ���������
  // � �����������
  //runAction->FillHist(detEnergy);
}
