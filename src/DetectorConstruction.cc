#include "DetectorConstruction.hh"
#include "DetectorSD.hh"

#include "G4Material.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "G4RotationMatrix.hh"
#include "G4NistManager.hh"
#include "globals.hh"
#include "G4VisAttributes.hh" 
#include "G4SDManager.hh"
#include "G4SubtractionSolid.hh"
#include "ParametersManager.hh"

//�������� ����� ������������ ���� ����� � ������ ���������
#define SAMPLE_SIZE 15
#define DETECTOR_SIZE 0.25
#define SAMPLE_TRANSLATION 0

using namespace CLHEP;

DetectorConstruction::DetectorConstruction() {}

DetectorConstruction::~DetectorConstruction() {}

G4VPhysicalVolume* DetectorConstruction::Construct()
{
    
    // --- materials ---
    
    // ������� ���������
    // ������ ������:
    //G4Element* N = new G4Element("Nitrogen", "N", 7, 14.01*g/mole);
    //G4Element* O = new G4Element("Oxygen"  , "O", 8, 16.00*g/mole);
    //G4Material* Air = new G4Material("Air", 1.29*mg/cm3, 2);
    //Air->AddElement(N, 70*perCent);
    //Air->AddElement(O, 30*perCent);
  
    // ������ ������: ���������� ���������� � Geant4 ���� ����������
    // ����� �������, �� ������ ���������� ��������� � ������� �������,
    // �.�. �� ��� ��������� ���������� � ����
    G4NistManager* nistMan = G4NistManager::Instance();
    G4Material* Air = nistMan->FindOrBuildMaterial(paramManager->get_nameAir());
    G4Material* saMaterial = nistMan->FindOrBuildMaterial(paramManager->get_nameSaMaterial());
    G4Material* detMaterial = nistMan->FindOrBuildMaterial(paramManager->get_nameDetMaterial());

    // ������ ������� ������� � ���������
    double saSize = SAMPLE_SIZE; // ������ �������
    double detSize = DETECTOR_SIZE; // ������ ���������
    double saTrans = SAMPLE_TRANSLATION; // ���������� �� ������ ��������� �� �������
    double detTrans = paramManager->get_detTrans(); // ���������� �� ������ ��������� �� ���������
    
    // ������� ����� � ���� ���������������
    //G4Box* world_box = new G4Box("world", (saSize + detSize)/2 + 1*cm, (saSize + detSize)/2 + 1*cm, gap1 + gap2 + detSize/2 + 1*cm);
    G4Box* world_box = new G4Box("world", 150*cm, 150*cm, 150*cm);
    // ��������� ��� ��������
    G4LogicalVolume* world_log = new G4LogicalVolume(world_box, Air, "world");
    // � �������� � ������ ���������
    G4VPhysicalVolume* world_phys = new G4PVPlacement(0, G4ThreeVector(), world_log, "world", 0, false, 0);

    // ������� �������� ����� �������
    G4Box* sample_box = new G4Box("Big", saSize*cm, saSize*cm, saSize*cm);
  
    // ������� �����, ������������ �� ��������� (������ ������ ������ ���������)
    G4Box* det_box = new G4Box("Small", detSize*cm, detSize*cm, detSize*cm);
  
    //�������� ������
    G4SubtractionSolid* sample = new G4SubtractionSolid("Sample", sample_box, det_box, 0, G4ThreeVector(detTrans*cm,0,0));
  
    // �� ����������� ���������� ���������� �������
    G4LogicalVolume* sample_log = new G4LogicalVolume(sample, saMaterial, "sample");
    // �������� ��� � ������� ����� �� ��������� �� saTrans
    G4VPhysicalVolume* sample_phys = new G4PVPlacement(0, G4ThreeVector(saTrans*cm, 0, 0), sample_log, "sample", world_log, false, 0);
  
    // ��������
    G4Box* detector = new G4Box("Detector", detSize*cm, detSize*cm, detSize*cm);
    G4LogicalVolume* det_log = new G4LogicalVolume(detector, detMaterial, "detector");
    // �������� ��� � ������� ����� �� ��������� �� detTrans
    G4VPhysicalVolume* det_phys = new G4PVPlacement(0, G4ThreeVector(detTrans*cm, 0, 0), det_log, "detector", world_log, false, 0);


    // --- sensitive detectors ---
    
    // ������������ det_log ��� ��������
    // ������ ��� ����������� ������ ����� ���� ����� ����� ����������
    // ����� DetectorSD::ProcessHits() � ������� �� ����� ��������������
    // �������, ����������� � ���������
    DetectorSD* detectorSD = new DetectorSD("DetectorSD");
    G4SDManager* sdMan = G4SDManager::GetSDMpointer();
    sdMan->AddNewDetector(detectorSD);
    det_log->SetSensitiveDetector(detectorSD);
  
    
    // --- visualisation ---
    
    // ��������� ����������� �������� ������
    world_log->SetVisAttributes(G4VisAttributes::Invisible);
    
    // � ��� ������� � ��������� ������ ����� �����������
    // (�� ��������� ��� �������� ��������� �������� � ����� ����)
    //sample_log->SetVisAttributes(G4VisAttributes::G4VisAttributes(G4Colour::Yellow()));
    //det_log->SetVisAttributes(G4VisAttributes::G4VisAttributes(G4Colour::Blue()));
    
    // ���������� ��������� �� ������� �����
    return world_phys;
}
