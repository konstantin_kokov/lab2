#include "PrimaryGeneratorAction.hh"
#include "Randomize.hh"
#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4RandomDirection.hh"
#include "G4GeneralParticleSource.hh"

PrimaryGeneratorAction::PrimaryGeneratorAction()
{
  // ������� �������� ������

  //G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  //G4ParticleDefinition* gamma = particleTable->FindParticle("gamma");
  // ������������� ��� � ������� ������, ���������� ��������� ���������
  //particleGun->SetParticleDefinition(gamma);
  //particleGun->SetParticleEnergy(100*keV);
  //particleGun->SetParticlePosition(G4ThreeVector(0, 0, 0));

  particleGun = new G4GeneralParticleSource();
  
}



PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
  
  delete particleGun;
}

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* event)
{
  // ������ ��������� ����������� ���������
  //particleGun->SetParticleMomentumDirection(G4RandomDirection());
  // �������� ��������� ���� �������
  particleGun->GeneratePrimaryVertex(event);
}

