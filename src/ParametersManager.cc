#include "ParametersManager.hh"

ParametersManager *paramManager = NULL;

ParametersManager::ParametersManager() :
  m_detTrans(DET_TRANS_DEFAULT),
  m_nameAir(strdup(NAME_AIR_DEFAULT)),
  m_nameSaMaterial(strdup(NAME_SA_MATERIAL_DEFAULT)),
  m_nameDetMaterial(strdup(NAME_DET_MATERIAL_DEFAULT))
{}

ParametersManager::~ParametersManager()
{
  if (m_nameAir)
    free(m_nameAir);
  if (m_nameSaMaterial)
    free(m_nameSaMaterial);
  if (m_nameDetMaterial)
    free(m_nameDetMaterial);
}

void ParametersManager::set_detTrans(double detTrans)
{
  m_detTrans = detTrans;
}

double ParametersManager::get_detTrans(void) const
{
  return m_detTrans;
}

void ParametersManager::set_nameAir(const char *nameAir)
{
  if (m_nameAir)
    free(m_nameAir);
  m_nameAir = strdup(nameAir);
}

char *ParametersManager::get_nameAir(void) const
{
  return m_nameAir;
}

void ParametersManager::set_nameSaMaterial(const char *nameSaMaterial)
{
  if (m_nameSaMaterial)
    free(m_nameSaMaterial);
  m_nameSaMaterial = strdup(nameSaMaterial);
}

char *ParametersManager::get_nameSaMaterial(void) const
{
  return m_nameSaMaterial;
}

void ParametersManager::set_nameDetMaterial(const char *nameDetMaterial)
{
  if (m_nameDetMaterial)
    free(m_nameDetMaterial);
  m_nameDetMaterial = strdup(nameDetMaterial);
}

char *ParametersManager::get_nameDetMaterial(void) const
{
  return m_nameDetMaterial;
}
