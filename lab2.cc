// ������������ ����� ��� ������� ������� �� ����������� ����
// ��� ����� ����� � ����������� include/
//include <cstdlib>
//#include <iostream>
//#include <sstream>
//#include <strstream>
#include "DetectorConstruction.hh"
#include "PhysicsList.hh"
#include "PrimaryGeneratorAction.hh"
#include "RunAction.hh"
#include "G4GeneralParticleSource.hh"
#include "G4SubtractionSolid.hh"
// ������������ ����� ��� ������� �� ���������� Geant4
#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "Randomize.hh"
// ���� � �������� ��� ������ � ��������� ��������
#include <ctime>
#ifdef G4VIS_USE
#include "G4VisExecutive.hh"
#endif

#ifdef G4UI_USE
#include "G4UIExecutive.hh"
#endif

#include "ParametersManager.hh"

#define EXPECTED_PARAM_NUMBER_BIG   6
#define EXPECTED_PARAM_NUMBER_SMALL 2

//double d1 = 5;

int main(int argc, char** argv)
{
  paramManager = new ParametersManager();
  if (argc == EXPECTED_PARAM_NUMBER_BIG)
  {
    printf("Setting parameters from command line.\n");
    double detTransRead;
    sscanf(argv[2], "%lf", &detTransRead);
    paramManager->set_detTrans(detTransRead);
    paramManager->set_nameAir(argv[3]);
    paramManager->set_nameSaMaterial(argv[4]);
    paramManager->set_nameDetMaterial(argv[5]);
  }
  else if (argc == EXPECTED_PARAM_NUMBER_SMALL)
  {
    printf("Setting parameters from default values in source code.\n");
  }
  else
  {
    printf("Error: wrong number of input parameters.\n");
    delete paramManager;
    exit(EXIT_FAILURE);
  }
  
  // ����� ���������� ��������� �����
  CLHEP::HepRandom::setTheEngine(new CLHEP::RanecuEngine);
  // � ������������� ���������� ������� ��������� �������
  // ��������� ����� ���������� ����� ���� �������� ������������� �� ����� ��������� ��������� 
  CLHEP::HepRandom::setTheSeed(time(NULL));
  
  // �������� ������ ��� ���������� ��������������
  G4RunManager* runManager = new G4RunManager;

  // ����������� ������������ �������: �������� ������, ���������, ��������� � ���������
  runManager->SetUserInitialization(new PhysicsList);
  runManager->SetUserInitialization(new DetectorConstruction);
  runManager->SetUserAction(new PrimaryGeneratorAction);

  // ����������� �������������� �������: �����/���������� ����������
  runManager->SetUserAction(new RunAction);

  // �������� � ��������� ������ ��� ���������� �������������
  G4VisManager* visManager = new G4VisExecutive;
  visManager->Initialize();
  
  // ��������� ���� �������������:
  // ���������������� ��������������� ������, ��������� � ���������
  // ������ �������, ����������� ���������, ...
  runManager->Initialize();
  
  // ������ ����������������� �����
  G4UImanager* UI = G4UImanager::GetUIpointer();
  
  printf("ExecuteMacroFile begins here...\n");
  // � argv[1] ���������� ������ �������� ��������� ������ (��� ����������������� �����)
  UI->ExecuteMacroFile(argv[1]);
  
  //std::stringstream(argv[1]) >> d1;
  
  // ������������ ������
  delete visManager;
  delete runManager;
  delete paramManager;
  
  // � �����
  return 0;
}
